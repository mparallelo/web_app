Meteor.publish('directory', function() {
  return Meteor.users.find({});
});

Meteor.publish('allowedEmailAddresses', function(currentNeighborhood) {
  //console.log('Meteor.publish allowedEmailAddresses');
  //console.log('currentNeighborhood');
  //console.log(currentNeighborhood);
  //const user = Meteor.users.findOne(this.userId);
  //if (user) {
  //  const userEmail = user.emails[0].address;
  return AllowedEmails.find({
    //neighborhood: currentNeighborhood, 
    email: {$ne: 'admin@mpowr.us'}
  });
});

Meteor.publish('cameras', function(currentNeighborhood) {
  //console.log('currentNeighborhood');
  //console.log(currentNeighborhood);
  //let neighborhood = Neighborhoods.find({name: currentNeighborhood});
  //return neighborhood.cameras;
  return Cameras.find({neighborhood: currentNeighborhood});
/*
  const user = Meteor.users.findOne(this.userId);
  if (user) {
    const userEmail = user.emails[0].address;
   } else {
    return this.ready();
  }*/
});

Meteor.publish('videos', function(cameraId, minDate, maxDate) {
  console.log('Meteor.publish(videos)');
  const user = Meteor.users.findOne(this.userId);
  const userEmail = user.emails[0].address;
  let cutoffDate = new Date();
  if (userEmail == 'admin@mpowr.us') {
      cutoffDate.setDate(cutoffDate.getDate()-30);
  } else {
    cutoffDate.setDate(cutoffDate.getDate()-7);
  }
  if (minDate.getTime() < cutoffDate.getTime()) {
    minDate = cutoffDate;
  }
  //console.log(cameraId);
  //console.log(minDate);
  //console.log(maxDate);
  let videos = Videos.find({$and: [{cameraId: cameraId}, {start: {$gt: minDate}}, {start: {$lt: maxDate}}] }, {sort: {start: 1}}).fetch();
  //console.log(videos);
  return Videos.find({$and: [{cameraId: cameraId}, {start: {$gt: minDate}}, {start: {$lt: maxDate}}] }, {sort: {start: 1}});
  //return Videos.find({start: {$gt: cutoffDate}}, {sort: {start: 1}});
});
/*
Meteor.publish('videos', function() {
  //var timelineMaxDate = new Date();
  //var timelineMinDate = new Date(timelineMaxDate);
  //timelineMinDate.setDate(timelineMaxDate.getDate()-1);
  //return Videos.find({start: {$gt: timelineMinDate}}, {sort: {start: 1}});
  cutoffDate = new Date();
  cutoffDate.setDate(cutoffDate.getDate()-7);
  return Videos.find({start: {$gt: cutoffDate}}, {sort: {start: 1}});
});
*/

Meteor.publish('neighborhoods', function() {
  //console.log('Server: neighborhoods');
  const user = Meteor.users.findOne(this.userId);
  //console.log(user);
  if (user) {
      const userEmail = user.emails[0].address;
      //console.log(userEmail);
      //console.log(Neighborhoods.find({}).fetch());//{emails: userEmail}).fetch());
      let neighborhoods = Neighborhoods.find({emails: userEmail}, {sort: {name: -1}});//.map(function(neighborhood, index) { neighborhood.idx = index; return neighborhood; });
      if (!neighborhoods.count()) {
        console.log('Could not find any neighborhood for user with id ' + this.userId);
      }
      return neighborhoods;
  } else {
    //console.log('Could not find any user with id ' + this.userId);
    return this.ready();
  }
});
/*
Meteor.publish('initial-neighborhood', function() {
    const user = Meteor.users.findOne(this.userId);
    const userEmail = user.emails[0].address;
    return Neighborhoods.find({emails: userEmail});

});
*/
