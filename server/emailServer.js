var rootURL = process.env.ROOT_URL;
console.log(process.env.ROOT_URL);

Accounts.emailTemplates.from = "mpowr.us <no-reply@mpowr.us>";
//Accounts.emailTemplates.from = "<postmaster@mymail.mpowr.us>";
Accounts.emailTemplates.siteName = "mpowr.us";
Accounts.emailTemplates.verifyEmail = {
  subject() {
    return "[mpowr.us] Verify Your Email Address";
  },
  text(user, url) {
    //console.log('Accounts.emailTemplates.verifyEmail');
    //console.log(url);
    let emailAddress   = user.emails[0].address,
        urlWithoutHash = url.replace( '#/', '' ),
        supportEmail   = "support@mpowr.us",
        emailBody      = 'To verify your email address (' + emailAddress
          + ') visit the following link:\n\n' + urlWithoutHash
          + '\n\n If you did not request this verification, please ignore this email. '
          + 'If you feel something is wrong, please contact our support team at ' + supportEmail + '.';
    return emailBody;
  }
};
Accounts.emailTemplates.resetPassword.subject = function (user) {
  return "Message for " + user.emails[0].address;
};

Accounts.emailTemplates.resetPassword.text = function (user, url) {
  var signature = "mpowr.us";
  var urlWithoutHash = url.replace( '#/', '' );
  return "Dear " + user.emails[0].address + ",\n\n" +
    "Click the following link to set your new password:\n" +
    urlWithoutHash + "\n\n" +
    "Cheers,\n" +
    signature
  ;
};
