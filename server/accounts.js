Accounts.validateNewUser(function (user) {
  var userEmail = user.emails[0].address;
  //if (userEmail == 'admin@mpowr.us') {
  //   return true;
  //}
  var emailIsAllowed = AllowedEmails.findOne({email: userEmail});
  if (emailIsAllowed) {
    return true;
  }
  throw new Meteor.Error(403, "This email address is not part"
    + " of the community list. If you think it should be added"
    + " to the list, please send a request to admin@mpowr.us"
  );
});

Meteor.users.allow({
  remove: function(userId, doc) {
    return true;
  }/*,
  fetch: []*/
});

Meteor.startup(function () {
  /*
  var user = Meteor.users.findOne({'emails.0.address': 'a@a'});
  if (!user) {
    user = {
      email: 'a@a',
      password: '>>/?',
    };
    userId = Accounts.createUser(user);
  }
  else {
    userId = user._id;
  }
  Meteor.users.update(userId, {$set: {'emails.0.verified': true}});
*/
  var user = Meteor.users.findOne({'emails.0.address': 'admin@mpowr.us'})
  if (!user) {
    user = {
      email: 'admin@mpowr.us',
      password: '>>/?',
    };
    userId = Accounts.createUser(user);
  }
  else {
    userId = user._id;
  }
  Meteor.users.update(userId, {$set: {'emails.0.verified': true}})
});
