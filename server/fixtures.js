
console.log('\n===================================================================');
console.log('=====================   Fixtures   ================================');
console.log('===================================================================');

if (process.env.ROOT_URL === 'http://localhost:3000/') {
    console.log('This is a local environment');
    //Neighborhoods.remove({});
    //AllowedEmails.remove({});
    //Cameras.remove({});
    //Videos.remove({});
    //Images.remove({});

    let today = new Date();

    if (Images.find().count() == 0) {
        Images.insert({
            filename: 'DHC-cam1-prodserver-image-2017-01-08T11-58-16.jpg',
            start: new Date(today.setHours(7)), 
            url: 'https://s3-us-west-1.amazonaws.com/mp-dhc-images/DHC-cam1-prodserver-image-2017-01-08T11-58-16.jpg',
            cameraId: 'DHC-cam1-prodserver'
        });
    }

    if (Videos.find().count() == 0) {
        Videos.insert({
            filename: 'axis1427.stream_9982555.mp4',
            start: new Date('Wed Nov 16 2016 09:46:22 GMT-0700 (PDT)'), 
            url: 'rtmp://184.72.27.200:1935/vods3/_definst_/mp4:amazons3/mp-dhc-video-data/axis1427.stream_9982555.mp4',
            cameraId: 'AX-ACCC8E0C4267'
        });
        Videos.insert({
            filename: 'axis1025.stream_24043600.mp4',
            start: new Date('Wed Nov 16 2016 05:40:43 GMT-0700 (PDT)'), 
            url: 'rtmp://54.219.101.14:1935/vods3/_definst_/mp4:amazons3/mp-dhc-video-data/axis1427.stream_24043600.mp4',
            cameraId: 'AX-ACCC8E0C4267'
        });
        Videos.insert({
            filename: 'axis1025.stream_79420520.mp4',
            start: new Date('Wed Nov 16 2016 01:03:40 GMT-0700 (PDT)'), 
            url: 'rtmp://54.219.101.14:1935/vods3/_definst_/mp4:amazons3/mp-dhc-video-data/axis1025.stream_79420520.mp4',
            cameraId: 'AX-ACCC8E0C4267'
        });
        Videos.insert({
            filename: 'axis1025.stream_79420520.mp4',
            start: new Date('Wed Nov 15 2016 19:03:40 GMT-0700 (PDT)'), 
            url: 'rtmp://54.219.101.14:1935/vods3/_definst_/mp4:amazons3/mp-dhc-video-data/axis1025.stream_79420520.mp4',
            cameraId: 'AX-ACCC8E0C4267'
        });
        Videos.insert({
            filename: 'axis1025.stream_79420520.mp4',
            start: new Date('Wed Nov 15 2016 10:03:40 GMT-0700 (PDT)'), 
            url: 'rtmp://54.219.101.14:1935/vods3/_definst_/mp4:amazons3/mp-dhc-video-data/axis1025.stream_79420520.mp4',
            cameraId: 'AX-ACCC8E0C4267'
        });

        Videos.insert({
            filename: 'axis1427.stream_9982555.mp4',
            start: new Date('Wed Nov 17 2016 19:46:22 GMT-0700 (PDT)'), 
            url: 'rtmp://184.72.27.200:1935/vods3/_definst_/mp4:amazons3/mp-dhc-video-data/axis1427.stream_9982555.mp4',
            cameraId: 'AX-ACCC8E0C4267'
        });
        Videos.insert({
            filename: 'axis1427.stream_9982555.mp4',
            start: new Date('Fri Dec 7 2016 19:40:22 GMT-0700 (PDT)'), 
            url: 'rtmp://184.72.27.200:1935/vods3/_definst_/mp4:amazons3/mp-dhc-video-data/axis1427.stream_9982555.mp4',
            cameraId: 'AX-ACCC8E0C4267'
        });
        Videos.insert({
            filename: 'axis1427.stream_65292747.mp4',
            start: new Date('Fri Dec 8 2016 19:00:22 GMT-0700 (PDT)'), 
            url: 'rtmp://54.219.101.14:1935/vods3/_definst_/mp4:amazons3/mparallelovideodata/axis1025.stream_61013525.mp4',
            cameraId: 'AX-ACCC8E0C4267'
        });
        Videos.insert({
            filename: 'axis1427.stream_65292747.mp4',
            start: new Date('Fri Dec 8 2016 18:30:22 GMT-0700 (PDT)'), 
            url: 'rtmp://54.219.101.14:1935/vods3/_definst_/mp4:amazons3/mparallelovideodata/axis1025.stream_61013525.mp4',
            cameraId: 'AX-ACCC8E0C4267'
        });
        Videos.insert({
            filename: 'axis1427.stream_65292747.mp4',
            start: new Date('Fri Dec 8 2016 23:10:22 GMT-0700 (PDT)'), 
            url: 'rtmp://54.219.101.14:1935/vods3/_definst_/mp4:amazons3/mparallelovideodata/axis1025.stream_61013525.mp4',
            cameraId: 'AX-ACCC8E0C4267'
        });
        Videos.insert({
            filename: 'axis1427.stream_65292747.mp4',
            start: new Date('Fri Dec 9 2016 04:10:22 GMT-0700 (PDT)'), 
            url: 'rtmp://54.219.101.14:1935/vods3/_definst_/mp4:amazons3/mparallelovideodata/axis1025.stream_61013525.mp4',
            cameraId: 'AX-ACCC8E0C4267'
        });
        Videos.insert({
            filename: 'axis1427.stream_65292747.mp4',
            start: new Date('Fri Dec 9 2016 06:00:22 GMT-0800 (PST)'), 
            url: 'rtmp://54.219.101.14:1935/vods3/_definst_/mp4:amazons3/mparallelovideodata/axis1025.stream_61013525.mp4',
            cameraId: 'AX-ACCC8E0C4267'
        });
    }
}

if (process.env.ROOT_URL === 'http://test.mpowr.us/') {
    console.log('This is a test environment');
    //Neighborhoods.remove({});
    //AllowedEmails.remove({});
    //Cameras.remove({});
    //Videos.remove({});
}

/*

var neighborhoods = JSON.parse(Assets.getText('Neighborhoods.json'));
if (Neighborhoods.find().count() == 0) {
    neighborhoods.forEach(function(nbh) {
        //console.log(nbh);
        nbh.emails.push(nbh.adminEmail);
        nbh.emails.push('admin@mpowr.us');
        Neighborhoods.insert(nbh);
    });
}

if (AllowedEmails.find().count() == 0) {
    neighborhoods.forEach(function(nbh) {
        AllowedEmails.upsert(
            { email: 'admin@mpowr.us' },
            { $push: { neighborhood: nbh.name} }
        );
        AllowedEmails.upsert(
            { email: nbh.adminEmail },
            { $push: { neighborhood: nbh.name} }
        );
        nbh.emails.forEach(function(email) {
                AllowedEmails.upsert(
                    { email: email },
                    { $push: { neighborhood: this.name} }
                );
        //    }
        }, nbh);
    });
}

if (Cameras.find().count() == 0) {
    neighborhoods.forEach(function(nbh) {
        nbh.cameras.forEach(function(camera) {
            camera.neighborhood = this.name;
            camera.width = 16;
            camera.height = 9;
            Cameras.insert(camera);
        }, nbh);
    });
}*/
/*
var newNeighborhoods = JSON.parse(Assets.getText('NewNeighborhoods.json'));
newNeighborhoods.forEach(function(nbh) {
    //console.log(nbh);
    nbh.emails.push(nbh.adminEmail);
    nbh.emails.push('admin@mpowr.us');
    Neighborhoods.insert(nbh);
    
    nbh.emails.forEach(function(email) {
        AllowedEmails.upsert(
            { email: email },
            { $push: { neighborhood: this.name} }
        );
    }, nbh);

    nbh.cameras.forEach(function(camera) {
        camera.neighborhood = this.name;
        camera.width = 16;
        camera.height = 9;
        Cameras.insert(camera);
    }, nbh);
});
*/


/*
console.log('\nNeighborhoods');
console.log(Neighborhoods.find().fetch());

console.log('\nAllowedEmails');
console.log(AllowedEmails.find().fetch());

console.log('\nCameras');
console.log(Cameras.find().fetch());

console.log('\nVideos');
//console.log(Videos.find().fetch());
console.log(Videos.findOne()); 
console.log('-------------------------------------------------------------------');
*/




