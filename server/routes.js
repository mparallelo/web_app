var bodyParser = require('body-parser');
var moment = require('moment-timezone');
 

Picker.middleware(bodyParser.json());
Picker.middleware(bodyParser.urlencoded({extended: false}));

Picker.route('/support-email', function(params, request, response, next) {
  //console.log('/support-email');
  //console.log(request.body);

  let mailFields = {};
  mailFields.to = request.body.sender;
  mailFields.from = "mpowr.us <no-reply@mpowr.us>";
  mailFields.subject = 'Re: ' + request.body.subject;
  mailFields.text = 'Dear ' + mailFields.to + '\n\nThank you for contacting us! Your email has been received and a member of the support team will get in touch with you soon.\n\nThanks again,\nmpowr.us';
  //mailFields.html = 'Thank you for contacting us!';//<br> Your email has been received and a member of the support team will get in touch with you soon.<br><br>Thanks again,<br>mpowr.us Support Team';
  Email.send({
      to: mailFields.to,
      from: mailFields.from,
      subject: mailFields.subject,
      text: mailFields.text,
      html: mailFields.html
  });
  //console.log("email sent!");
  //console.log(mailFields);
  response.statusCode = 200;
  response.end();
});

Picker.route('/remove-video', function(params, request, response, next) {
  //console.log('/remove-video');
  //console.log(request.body);
  var filename = request.body.filename.split('/').pop();
  var cameraId = request.body.cameraid;
  //console.log('Filename ' + filename);
  //Videos.remove({cameraId: cameraId, filename: filename});

  response.statusCode = 200;
  response.end();
});

Picker.route('/add-video', function(params, request, response, next) {
  //console.log('/add-video request.body');
  //console.log(request.body);
  const cameraId = request.body.cameraid;
  //console.log('cameraId ' + cameraId);
  const camera = Cameras.findOne({id: cameraId});
  if (!camera) {
    console.log('ERROR: wrong camera id: ' + cameraId);
    response.end();
    return;
  }
  const timezone = camera.timezone || 'GMT';
  console.log(timezone);
  console.log(moment.tz(request.body.starttime, "America/Los_Angeles").format());
  let date = new Date(moment.tz(request.body.starttime, "America/Los_Angeles").format());
  //let date = new Date(Date.parse(request.body.starttime));
  //date.setHours(date.getHours()+8);
  console.log(date);
  //let cutoutDate = new Date(date).setDate(date.getDate()-8);
  if (camera) {
    Videos.insert({
      start: date, 
      cameraId: cameraId
    });
  }
  //Videos.remove({start: {$lt: cutoutDate}});
  response.statusCode = 200;
  response.end();
});

Picker.route('/add-image', function(params, request, response, next) {
  //console.log('/add-image request.body');
  console.log(request.body);
  const cameraId = request.body.cameraid;
  const camera = Cameras.findOne({id: cameraId});
  if (!camera) {
    console.log('ERROR: wrong camera id: ' + cameraId);
    response.end();
    return;
  }
  const timezone = camera.timezone || 'GMT';
  //console.log(timezone);
  //console.log(moment.tz(request.body.starttime, "America/Los_Angeles").format());
  let date = new Date(moment.tz(request.body.starttime, "America/Los_Angeles").format());
  //console.log(date);
  let imageType = request.body.type;
  //console.log(imageType);
  if (camera) {
    Images.insert({
      start: date, 
      cameraId: cameraId,
      type: imageType
    });
  }
  //Images.remove({start: {$lt: cutoutDate}});
  response.statusCode = 200;
  response.end();
});