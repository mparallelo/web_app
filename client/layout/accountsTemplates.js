import { Template } from 'meteor/templating';
import './accountsTemplates.html';

// 
// We override some of the account templates
//---------------------------------------------------------------------------------
Template['override-atForm'].inheritsHelpersFrom("atForm");
Template['override-atForm'].replaces('atForm');
Template['override-atPwdForm'].replaces('atPwdForm');
Template['override-atNavButton'].replaces('atNavButton');
Template['override-atError'].replaces('atError');
//Template['override-atPwdFormBtn'].replaces('atPwdFormBtn');
//Template['override-atTermsLink'].replaces('atTermsLink');
//Template['override-atSigninLink'].replaces('atSigninLink');


Template['atForm'].helpers({
  showForgotPasswordLink: function() {
    var parentData = Template.currentData();
    var state = (parentData && parentData.state) || AccountsTemplates.getState();
    return state === "signIn" && AccountsTemplates.options.showForgotPasswordLink;
  },
  waitingForVerification: function() {
    var parentData = Template.currentData();
    var state = (parentData && parentData.state) || AccountsTemplates.getState();
    console.log('state = '+ state);
    return state === "verifyEmail";
  },
  registrationInProgress: function() {
    var parentData = Template.currentData();
    var state = (parentData && parentData.state) || AccountsTemplates.getState();
    console.log('state = '+ state);
    return state === "verifyEmail" || state === "sendingVerificationEmail";
  },
  showResendVerificationEmailLink: function(){
    var parentData = Template.currentData();
    var state = (parentData && parentData.state) || AccountsTemplates.getState();
    var errors = AccountsTemplates.state.form.get("error");
    return errors && errors[0].includes(AccountsTemplates.texts.errors.verifyEmailFirst);
  },
  emailAlreadyExists: function(){
    var errors = AccountsTemplates.state.form.get("error");
    return errors && errors[0].includes("Email already exists");
  }
});
Template.atForm.events({
  'click .at-btn': function(){
    //console.log('click .at-btn');
    var parentData = Template.currentData();
    var state = (parentData && parentData.state) || AccountsTemplates.getState();
    if (state == 'resendVerificationEmail') {
      console.log('click .at-btn resendVerificationEmail');
    }
  }
});

Template['override_atSignupLink'].inheritsHelpersFrom("atSignupLink");
Template['override_atSignupLink'].helpers({
  linkText: function() { console.log('linkText'); return "Create one"; }
});

Template.atTermsLink.events({
  'click a': function(event){
    console.log('click atTermsLink');
    console.log('event');
    console.log(event);
    if (event.target.innerText === 'Terms of Use') {
      $('#termsPopup').modal();
    }
    else if (event.target.innerText === 'Privacy Policy') {
      $('#privacyPopup').modal();
    }
    event.preventDefault();
  }
});