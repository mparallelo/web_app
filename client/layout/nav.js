function currentNeighborhood() {
    let currentNeighborhood = Session.get('currentNeighborhood');
    if (!currentNeighborhood) {
      let aNeighborhood = Neighborhoods.findOne();
      if (aNeighborhood) {
      currentNeighborhood = aNeighborhood.name;
        Session.set('currentNeighborhood', currentNeighborhood);
      } else {
        console.log('Error: currentNeighborhood should be defined!');
      }
    }
    return currentNeighborhood;
}

Template.nav.helpers({
  adminUser: function() {
    let userEmail = Meteor.user().emails[0].address;
    if (userEmail === 'admin@mpowr.us') {
      return true;
    }
    return Neighborhoods.find({adminEmail: userEmail}).count() > 0;
  },
  splash: () => {
    return !Meteor.user();
  },
  neighborhoods() {
    return Neighborhoods.find({});
  },
  multipleNeighborhoods() {return Neighborhoods.find({}).count() > 1;},
  currentNeighborhood() {
    return currentNeighborhood();
  }
});

Template.nav.events({
  'click .dropdown-item': function (e, template) {
    Session.set('currentNeighborhood', e.target.innerText);
    //console.log(FlowRouter.getRouteName());
    //
    // When we change neighborhoods, timeline path becomes invalid, so we redirect to dashboard
    //
    if (FlowRouter.getRouteName().startsWith('timeline')) {
      FlowRouter.go('/dashboard');
    }
  }
});
