//import { ReactiveVar } from 'meteor/reactive-var';

function debugTimeline(arg) { console.log(arg); }
//function debugTimeline(arg) {}
var videoDataRows = [['date', 'sample', 'extra']];
Days = new Mongo.Collection(null);

//TimelineSubs = new SubsManager();

function dateToFilename(date) {
    const monthString   = ((date.getMonth()    < 9)? '0' : '') + String(date.getMonth()+1);
    const dayString     = ((date.getDate()    < 10)? '0' : '') + String(date.getDate());
    const hourString    = ((date.getHours()   < 10)? '0' : '') + String(date.getHours());
    const minutesString = ((date.getMinutes() < 10)? '0' : '') + String(date.getMinutes());
    const secondsString = ((date.getSeconds() < 10)? '0' : '') + String(date.getSeconds());
    return String(date.getFullYear()) + '-' + monthString + '-' + dayString + 'T' + hourString + '-' + minutesString + '-' + secondsString
}

function currentCamera() {
  return Cameras.findOne({nickname: FlowRouter.getParam('nickname')});
}
Template.timeline.onDestroyed(function () {
  Session.set('currentIndex', -1);
});

Template.barChart.helpers({
  myChartData() {
    debugTimeline('Template.barChart.helpers myChartData');
    var self = this;
    var xx = 0.4;
    var timelineMaxDate = Session.get('timelineMaxDate');
    var timelineMinDate = Session.get('timelineMinDate');
    var videos = Videos.find({$and: [{cameraId: currentCamera().id}, {start: {$gt: timelineMinDate}}, {start: {$lt: timelineMaxDate}}] }, {sort: {start: 1}});
    const videoCount = videos.count();
    Session.set('videoCount', videoCount);

    videoDataRows = [['date', 'sample', 'extra']];
    if (videoCount) {
      videos.forEach(function(video) {
        if (video.cameraId === currentCamera().id) {// && video.start.getTime() >= minDate.getTime() && video.start.getTime() <= maxDate.getTime()) {
          videoDataRows.push([video.start, xx, 0]);
        }
      });
    }

    var minDate = Session.get('timelineMinDate');
    var maxDate = Session.get('timelineMaxDate');
    if (minDate.getTime() < (new Date()).getTime() && maxDate.getTime() > (new Date()).getTime()) {
      videoDataRows.push([new Date(), xx, 0]);
    }
    debugTimeline(videoDataRows);
    let crtIndex = Session.get('currentIndex');
    debugTimeline('crtIndex: ' + crtIndex);
    if (crtIndex == undefined) {
      crtIndex = videoDataRows.length-2;
      //Session.set('currentIndex', crtIndex);
      gotoIndex(crtIndex);
    } else if (crtIndex == -1) {
      crtIndex = videoDataRows.length-2;
      //Session.set('currentIndex', crtIndex);
      gotoIndex(crtIndex);
    }
    for (var i = 1; i <= videoDataRows.length-1; i++) {
       videoDataRows[i][2] = 0;
       if (i == (crtIndex+1)) {
          videoDataRows[i][2] = 1;
       }
    }
    debugTimeline(videoDataRows);

    //updateCursor(videos.count());
    //Session.set('currentIndex', videos.count());
    //
    // Set up ticks
    //
    var tickValues = [];
    var tick = new Date(minDate);
    tick.setMinutes(0);
    tick.setSeconds(0);
    tick.setMilliseconds(0);
    while (tick <= maxDate) {
      if (tick >= minDate) {
        tickValues.push(tick.getTime());
      }
      tick.setHours(tick.getHours()+1);
    }

    // ------------------------------------------------------------------------------------------------
    // Create and return chart object
    // ------------------------------------------------------------------------------------------------
    returnObject = {
      size   : { height: 56 },
      //padding: { top: 20, right: 20, bottom: 20, left: 20},
      legend : { show: false },
      point  : {
        r      : function(d) { if (d.index == Session.get('currentIndex')) return 2.5; else return 2.5; },
        focus  : { expand: { r: 6 } },
        show   : function(d) { return !d.invisible; }
      },
      bar    : {
        width: 3,
        zerobased: false
      },
      axis : {
        x : {
          type : 'timeseries',
          min: minDate.getTime(),
          max: maxDate.getTime(),
          tick : {
            fit: false,
            format : function (x) { let hours = x.getHours(); return ((hours<10)? ' ' : '') + hours; },//"%H",//%H:%M",
            values : tickValues
          },
          padding: {
            left:  0,
            right: 0,
          }
        },
        y: {
          show: false,
          max: 1,
          min: 0,
          padding: {top: 0, bottom: 0}
        }
      },
      data: {
        rows: videoDataRows,
        types:  {
          sample: 'scatter',
          extra:  'bar',
        },
        x       : 'date',
        xFormat : '%Y%m%d',
        color: function (color, d) {
          var videoCount = Session.get('videoCount');
          var currentIndex = Session.get('currentIndex');
          if (d.index == videoCount && d.id == "sample") {
            return 'darkorange';
          } else if (d.id == "extra") {
            return 'red';
          } else {
            return 'steelblue';
          }
        },
        onclick: function (d, element) {
          debugTimeline(d);
          gotoIndex(d.index);
        }
      },
      tooltip: {
        contents: function (d, defaultTitleFormat, defaultValueFormat, color) {
          var date = new Date(d[0].x);
          var minutes = date.getMinutes();
          var text = '' + date.getMonth() + '/' + date.getDate() + '&nbsp;' + date.getHours() + ':' + ((minutes < 10)? '0' : '') + minutes;
          return '<span class="tooltip-text">' + text + '</span>';
        },
        position: function (data, width, height, element) {
          mouse = d3.mouse(element);
          return {top: -20, left: mouse[0]+0};
        }
      }
		};
    return returnObject;
	}
});







Template.timelineOrLoading.helpers({
  selectedCamera: function() {
    debugTimeline('selectedCamera');
    debugTimeline('nickname: ' + FlowRouter.getParam('nickname'));
    debugTimeline(currentCamera());
    if (!currentCamera()) return null;
    return currentCamera();
  },
})

Template.timelineOrLoading.onCreated(function() {
  debugTimeline('Template.timelineOrLoading.onCreated');
	this.autorun(() => {
	  this.subscribe('cameras', Session.get('currentNeighborhood'));
  });
});

Template.timelineView.onCreated(function() {
  debugTimeline('Template.timelineView.onCreated');
  //
  // Store max date and min date for displayed videos  
  //
  let timelineMinDate = new Date();
  timelineMinDate.setHours(0);
  timelineMinDate.setMinutes(0);
  timelineMinDate.setSeconds(0);
  timelineMinDate.setMilliseconds(0);
  Session.set('timelineMinDate', timelineMinDate);
  let timelineMaxDate = new Date(timelineMinDate);
  timelineMaxDate.setHours(timelineMaxDate.getHours()+24);
  timelineMaxDate.setSeconds(-1);
  Session.set('timelineMaxDate', timelineMaxDate);
  let cutoffDate = new Date();
  if (Meteor.user().emails[0].address == 'admin@mpowr.us') {
      cutoffDate.setDate(cutoffDate.getDate()-30);
  } else {
    cutoffDate.setDate(cutoffDate.getDate()-7);
  }
  Session.set('cutoffDate', cutoffDate);
  this.autorun(() => {
    let timelineMinDate = Session.get('timelineMinDate');
    let timelineMaxDate = Session.get('timelineMaxDate');
    let cutoffDate = Session.get('cutoffDate');
    debugTimeline(timelineMinDate);
    //this.subscribe('videos', currentCamera().id, Session.get('timelineMinDate'), Session.get('timelineMaxDate'));
    this.subscribe('videos', currentCamera().id, timelineMinDate, timelineMaxDate);
    Days.remove({});
    date = cutoffDate;
    crtDate = new Date();
    let index = 0;
    while (date.getTime() <= crtDate.getTime()) {
      Days.insert({date: date, index: index});
      date.setDate(date.getDate()+1);
      index += 1;
    }
  });
  Session.set('currentDateIndex', Days.find().count()-1);
});

//
// Setup player when timelineView is rendered
// ------------------------------------------------------------------------------------
Template.videoPlayer.rendered = function () {
  debugTimeline('Template.videoPlayer.rendered');
  debugTimeline(Session.get('currentIndex'));
  gotoIndex(Session.get('currentIndex'));
}
Template.videoPlayer.helpers({
  videosAvailable() {
    debugTimeline('videosAvailable');
    let timelineMinDate = Session.get('timelineMinDate');
    let timelineMaxDate = Session.get('timelineMaxDate');
    debugTimeline(Videos.findOne({$and: [{cameraId: currentCamera().id}, {start: {$gt: timelineMinDate}}, {start: {$lt: timelineMaxDate}}] }));
    const videoCount = Videos.find({$and: [{cameraId: currentCamera().id}, {start: {$gt: timelineMinDate}}, {start: {$lt: timelineMaxDate}}] }, {sort: {start: 1}}).count();
    return videoCount || (timelineMinDate.getTime() < (new Date()).getTime() && timelineMaxDate.getTime() > (new Date()).getTime());
  }
});


//
// Automatically play next video
// ------------------------------------------------------------------------------------
var playerOnComplete = function() {
  if (Session.get('currentIndex') < (videoDataRows.length-2)) {//} || nextDayEnabled() ) {
    let crtIndex = Session.get('currentIndex') + 1;
    gotoIndex(crtIndex);
  } else if (nextDayEnabled()) {
    gotoNextDay();
  }
}

//
// Handle navigation events
// ------------------------------------------------------------------------------------
Template.timelineView.events({
  'click #next-day-btn': function(event, template) {
    debugTimeline('next-day');
    let timelineMaxDate = Session.get('timelineMaxDate');
    timelineMaxDate.setDate(timelineMaxDate.getDate()+1);
    let timelineMinDate = Session.get('timelineMinDate');
    timelineMinDate.setDate(timelineMinDate.getDate()+1);
    if (timelineMinDate.getTime() < (new Date()).getTime()) {
      Session.set('timelineMaxDate', timelineMaxDate);
      Session.set('timelineMinDate', timelineMinDate);
      Session.set('currentIndex', 0);
    }
  },
  'click #prev-day-btn': function(event, template) {
    debugTimeline('prev-day');
    let timelineMaxDate = Session.get('timelineMaxDate');
    timelineMaxDate.setDate(timelineMaxDate.getDate()-1);
    let timelineMinDate = Session.get('timelineMinDate');
    timelineMinDate.setDate(timelineMinDate.getDate()-1);
    if (timelineMaxDate.getTime() > Session.get('cutoffDate')) {
      Session.set('timelineMaxDate', timelineMaxDate);
      Session.set('timelineMinDate', timelineMinDate);
      Session.set('currentIndex', 0);
    }
  },
  'click .btn.date': function(event, template) {
    debugTimeline('choose-date');
    debugTimeline(event.target.id);
    Session.set('currentDateIndex', event.target.id);
    let timelineMinDate = Session.get('cutoffDate');
    timelineMinDate.setHours(0);
    timelineMinDate.setMinutes(0);
    timelineMinDate.setSeconds(0);
    timelineMinDate.setMilliseconds(0);
    timelineMinDate.setDate(timelineMinDate.getDate() + Number(event.target.id));
    Session.set('timelineMinDate', timelineMinDate);
    let timelineMaxDate = new Date(timelineMinDate);
    timelineMaxDate.setHours(timelineMaxDate.getHours()+24);
    timelineMaxDate.setSeconds(-1);
    Session.set('timelineMaxDate', timelineMaxDate);
    Session.set('currentIndex', 0);
  }
});
Template.timelineNav.events({
  'click #next': function(event, template) {
      debugTimeline('next');
      const crtIndex = Session.get('currentIndex');
      if (crtIndex < videoDataRows.length-2) {//}      Session.get('videoCount')) {
        gotoIndex(crtIndex+1);
      } else {
        gotoNextDay();
      }
  },
  'click #previous': function(event, template) {
      debugTimeline('previous');
      const crtIndex = Session.get('currentIndex');
      if (crtIndex > 0) {
        gotoIndex(crtIndex-1);
      } else {
        gotoPreviousDay();
      }
  }
});

function flowURL(url) {
  let flowURL = url.replace('rtmp', 'http');
  if (!flowURL.endsWith('/playlist.m3u8')) {
    flowURL = flowURL.replace('rtmp', 'http').concat('/playlist.m3u8');
  }
	return flowURL;
}

//
// Move the cursor and play video at current index
// ------------------------------------------------------------------------------------
function gotoIndex(crtIndex) {
  debugTimeline('gotoIndex ' + crtIndex);
  Session.set('currentIndex', crtIndex);
  
  if (crtIndex > videoDataRows.length-2) return;
  if (crtIndex < 0) return;

  const crtVideo = getVideoForIndex(crtIndex);
  const camera = currentCamera();
  let url = undefined;
  if (crtVideo) {
    //url = crtVideo.url;
    //url = camera.storage + crtVideo.filename;

    url = camera.storage//streamingURL
        + camera.id
        + '-video-' + dateToFilename(crtVideo.start) + '.mp4/playlist.m3u8';
    debugTimeline('video found ...');
    //debugTimeline('crtIndex: ' + crtIndex);
    debugTimeline('url: ' + url);
  } else {
    url = camera.url;
    debugTimeline('url-cam = ' + url);
  }
  var container = document.getElementById("video");

  if (!container) return;
  debugTimeline(container);

  let player = flowplayer(container);
  if (player && player.error) {
    console.log('player.error');
    player.unload();
  }
  if (!player) {
    player = flowplayer(container, {
      autoplay: true,
      //live: true,
      ratio: 9/16,
      splash: true,
      clip: {
        sources: [
          { 
            type: "application/x-mpegurl",
            src:  flowURL(url)
          }
        ]
      }/*,
      embed: {
        skin: "//releases.flowplayer.org/6.0.5/skin/minimalist.css"
      }*/
    });
    player.on('finish', playerOnComplete);
    player.on('error', function (e, api, err) {
      let header = container.querySelector(".fp-message h2");
      let detail = container.querySelector(".fp-message p");
      if (err.code === 2 || err.code === 4) {
        header.innerHTML = "We are sorry, currently no live stream available.";
        //detail.innerHTML = " ";
      }
    });      
    player.toggle();

    /*player.on("error", function (e, api, err) {
      api.unload();
      / *const delay = 5;
      let header = container.querySelector(".fp-message h2");
      let detail = container.querySelector(".fp-message p");
      console.log('err');
      console.log(err);
      if (err.code === 2 || err.code === 4) {
        header.innerHTML = "We are sorry, currently no live stream available.";
        detail.innerHTML = "Retrying in <span>" + delay + "</span> seconds ...";
    }* /
    });*/
  } else {    //$('.fp-controls').show();
    console.log('player exists');
    player.unload(); 
    $('.fp-message').remove();
    player.error = player.loading = false;
    container.className = container.className.replace(/ *is-error */, "");
    container.className = container.className.replace(/ *is-finished */, "");
    player.load(flowURL(url));
  }
  $('.fp-embed').hide();
  $('.fp-embed-code').hide();
  //$(".fp-help").hide();
  //$('.fp-context-menu').remove();
  if (url == camera.url) {
   	$('.fp-controls').hide();
  	$('.fp-elapsed').hide();
	  $('.fp-duration').hide();
  }
  else {
   	$('.fp-controls').show();
  	$('.fp-elapsed').show();
	  $('.fp-duration').show();
  }
}
function getVideoForIndex(crtIndex) { 
  const crtDate = videoDataRows[crtIndex+1][0];
  return Videos.findOne({cameraId: currentCamera().id, start: crtDate});
}

function gotoNextDay() {
  debugTimeline('gotoNextDay');
  let timelineMaxDate = Session.get('timelineMaxDate');
  timelineMaxDate.setDate(timelineMaxDate.getDate()+1);
  let timelineMinDate = Session.get('timelineMinDate');
  timelineMinDate.setDate(timelineMinDate.getDate()+1);
  if (timelineMinDate.getTime() < (new Date()).getTime()) {
    Session.set('timelineMaxDate', timelineMaxDate);
    Session.set('timelineMinDate', timelineMinDate);
    Session.set('currentIndex', 0);
  }
}
function gotoPreviousDay() {
  debugTimeline('gotoPreviousDay');
  let timelineMaxDate = Session.get('timelineMaxDate');
  timelineMaxDate.setDate(timelineMaxDate.getDate()-1);
  let timelineMinDate = Session.get('timelineMinDate');
  timelineMinDate.setDate(timelineMinDate.getDate()-1);
  if (timelineMaxDate.getTime() > Session.get('cutoffDate')) {
        Session.set('timelineMaxDate', timelineMaxDate);
        Session.set('timelineMinDate', timelineMinDate);
        debugTimeline(timelineMinDate);
        debugTimeline(timelineMaxDate);
        //debugTimeline(Videos.find().fetch());
        //Session.set('currentIndex', Videos.find().count()-1);
        Session.set('currentIndex', -1);
  }
}

 
Template.timeline_noVideo.helpers({
  videoPlaceholderHeight: function() {
    const width = $('#timeline-view').width();
    return Math.round(width*9/16);
  }
});
//
// Navigation helpers
// ------------------------------------------------------------------------------------
Template.timelineView.helpers({
  leftDateText: function() {
    const minDateString = Session.get('timelineMinDate').toDateString();
    return minDateString.slice(0, minDateString.lastIndexOf(' '));
  },
  dateText: function(date) {
    //console.log(date);
    const dateString = date.date.toDateString();
    return dateString.slice(0, dateString.lastIndexOf(' '));
  },
  dateIndex: function(date) {
    const dateRecord = Days.findOne({date: date.date});
    //console.log(dateRecord); console.log('dateRecord');
    if (dateRecord) return dateRecord.index;
    else return null;
  },
  dateActive: function(date) {
    console.log('date'); console.log(date); 
    //return (date.index == Session.get('currentDateIndex'))? 'active' : '';
    let timelineMinDate = Session.get('timelineMinDate');
    let timelineMaxDate = Session.get('timelineMaxDate');
    return (date.date.getTime() >= timelineMinDate.getTime() && date.date.getTime() <= timelineMaxDate.getTime())? 'active' : '';
  },
  dates() {
    return Days.find();
  },
  nextDayDisabled: function() {
    if (nextDayEnabled()) {
      return '';
    } else {
      return 'disabled';
    }
  },
  prevDayDisabled: function() {
    if (prevDayEnabled()) {
      return '';
    } else {
      return 'disabled';
    }
  }
});
Template.timelineNav.helpers({
  previousDisabled() {
    if (Session.get('currentIndex') > 0 || prevDayEnabled()) {
      return '';
    } else {
      return 'disabled';
    } 
  },
  nextDisabled() {
    debugTimeline('nextDisabled');
    debugTimeline(videoDataRows);
    if (Session.get('currentIndex') < videoDataRows.length-2 || nextDayEnabled() ) {
      return '';
    } else {
      return 'disabled';
    } 
  },
  downloadDisabled() {
    if (Session.get('currentIndex') < Session.get('videoCount')) {
      return '';
    } else {
      return 'disabled';
    }
  },
  downloadURL() {
    if (videoDataRows.length == 1) return '';
    const crtIndex = Session.get('currentIndex');
    const crtDate = videoDataRows[crtIndex+1][0];
    const crtVideo = Videos.findOne({cameraId: currentCamera().id, start: crtDate});
    if (crtVideo) {
      //return currentCamera().download + crtVideo.filename;
      const downloadURL = currentCamera().download
          + currentCamera().id
          + '-video-' + dateToFilename(crtVideo.start) + '.mp4';
      return downloadURL;
    }
  }
});


function nextDayEnabled() {
  let nextDayMin = Session.get('timelineMinDate');
  nextDayMin.setDate(nextDayMin.getDate()+1);
  return (nextDayMin.getTime() < (new Date()).getTime());
}
function prevDayEnabled() {
  let prevDayMax = Session.get('timelineMaxDate');
  prevDayMax.setDate(prevDayMax.getDate()-1);
  return (prevDayMax.getTime() > Session.get('cutoffDate').getTime());
}
