function flowURL(url) {
	let flowURL = url.replace('rtmp', 'http');
	if (!flowURL.endsWith('/playlist.m3u8')) {
		flowURL = flowURL.replace('rtmp', 'http').concat('/playlist.m3u8');
	}
	return flowURL;
}
/*
Template.dashboardOrLogin.events({
   'contextmenu *':function(e) {
      e.stopPropagation();
			console.log(this);
   }
});
*/
Template.dashboard.helpers({
	cameras: function() {
		return Cameras.find({}, {sort: {position: 1}});
	}
});

Template.dashboardOrLoading.onCreated(function() {
	this.autorun(() => {
	  this.subscribe('cameras', Session.get('currentNeighborhood'));
  });
});

Template.dashboard.onRendered(function() {});

function iOS() {
  return /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
}

function isSafari() {
  return navigator.userAgent.indexOf('Safari') != -1
      && navigator.userAgent.indexOf('Chrome') == -1;
}

Template.cameraThumbnail.onRendered(function () {
	if (!this.data) return;
  var container = document.getElementById("dashboard-" + this.data.nickname);
  flowplayer(container, {
 	  autoplay: true,
    ratio: 9/16,
		live: true,
    clip: {
      sources: [
				{ 
						type: "application/x-mpegurl",
						src:  flowURL(this.data.url)
				}
			]
    },
		plugins: {
			controls: null
		},
		embed: {
      skin: "//releases.flowplayer.org/6.0.5/skin/minimalist.css"
    }
  });
	$('.fp-embed').remove();
	$('.fp-fullscreen').remove();
	$('.fp-elapsed').remove();
	$('.fp-controls').hide();
	$('.fp-help').hide();
});

Template.cameraThumbnail.events({
  'click': function(event, template){
    //console.log('click cameraThumbnail');
		//console.log(event);
		//console.log(template);
		FlowRouter.go('/timeline/' + template.data.nickname);
	}
});
