Template.cameraTableOrLoading.events({
   'contextmenu *':function(e){
      e.stopPropagation();
			console.log(this);
   }
});

Template.cameraTable.helpers({
	cameras: function() {
		return Cameras.find({}, {sort: {position: 1}});
	}
});

Template.cameraTable.events({
  'click .deleteCamera': function(){
    Cameras.remove(this._id);
  }
});
Template.addCamera.events({
  'submit form': function(e, b){
		var newCamera = {
			nickname: $('#cameraNickname').val().trim(),
      id: $('#cameraId').val().trim(),
      position: $('#position').val().trim(),
      timezone: $('#timezone').val().trim(),
			url: $('#cameraURL').val().trim(),
      storage: $('#storageURL').val().trim(),
      download: $('#downloadURL').val().trim(),
      snapshot: $('#snapshotURL').val().trim(),
      neighborhood: Session.get('currentNeighborhood'),
			width: 16,//Number($('#cameraWidth').val().trim()),
			height: 9//Number($('#cameraHeight').val().trim()),
		}
    console.log(newCamera);
    Cameras.insert(newCamera);
    $('#addCameraForm').find('input:text').val('');
    $('#cameraNickname').focus();
    return false;
  }
});

Template.cameraTableOrLoading.onCreated(function() {
	this.autorun(() => {
    this.subscribe('cameras', Session.get('currentNeighborhood'));
  });
});
