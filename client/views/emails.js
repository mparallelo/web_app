
Template.allowedEmailsTableOrLoading.onCreated(function() {
	this.autorun(() => {
  	this.subscribe('allowedEmailAddresses', Session.get('currentNeighborhood'));
  });
});

Template.allowedEmailsTable.helpers({
	allowedEmails: function() {
		return AllowedEmails.find({neighborhood: Session.get('currentNeighborhood')});
	}
});

Template.allowedEmail.helpers({
	email: function() {
		return this.email;
	},
	passwordCreated: function() {
		user = Meteor.users.findOne({ 'emails.0.address': this.email });
		return !!user;
	},
	verified: function() {
		user = Meteor.users.findOne({ 'emails.0.address': this.email });
		return !!(user && user.emails[0].verified);
	},
});

Template.addEmail.events({
  'submit form': function(e, b){
		var newEmail = $('#userEmail').val().trim();
		Meteor.call('allowedEmails.add', {
			email: newEmail,
			neighborhood: Session.get('currentNeighborhood')
		}, (err, res) => {
			if (err) {
				alert(err);
			} else {
				// success!
			}
    });
    $('#addEmailForm').find('input:text').val('');
    $('#userEmail').focus();
    return false;
  }
});

Template.allowedEmail.events({
  'click .deleteEmail': function(){
    AllowedEmails.remove(this._id);
    user = Meteor.users.findOne({ 'emails.0.address': this.email });
		if (user) {
			Meteor.users.remove(user._id);
		} else {
			console.log('Could not find any user with email ' + this.email);
		}
  }
});

