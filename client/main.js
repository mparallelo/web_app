import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

Meteor.autorun(function() {
  if (!Meteor.user()) {
		$('body').addClass('splash')
  } else {
		$('body').removeClass('splash')
  }
});

Meteor.subscribe('directory'); //FIXME: validation should be done on sever
//Meteor.subscribe('videos'); //FIXME: subscribe in templates
Meteor.subscribe('neighborhoods'); //FIXME: subscribe in templates
