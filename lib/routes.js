
FlowRouter.route('/dashboard', {
  name: 'dashboard',
  triggersEnter: [AccountsTemplates.ensureSignedIn],
  action: function(params, queryParams) {
    console.log(params);
    console.log("FlowRouter.route('/dashboard'");
    console.log(Session.get('currentNeighborhood'));
    BlazeLayout.render('masterLayout', {
      footer: 'footer',
      main: 'dashboardOrLogin',
      nav: 'nav',
    });
  }
});
FlowRouter.route('/dashboard/:neighborhoodIdx', {
  name: 'dashboard',
  triggersEnter: [AccountsTemplates.ensureSignedIn],
  action: function(params, queryParams) {
    console.log(params);
    console.log("FlowRouter.route('/dashboard/:neighborhoodIdx'");
    console.log(Session.get('currentNeighborhood'));
    BlazeLayout.render('masterLayout', {
      footer: 'footer',
      main: 'dashboardOrLogin',
      nav: 'nav',
    });
  }
});

FlowRouter.route('/', {
  name: 'index',
  triggersEnter: [function(context, redirect) {
    if (Meteor.user()) {
      redirect('/dashboard');
    }
    else {
      redirect('/sign-in');
    }
  }],
  action: function(_params) {
    throw new Error("this should not get called");
  }
});
/*
FlowRouter.route('/single', {
  name: 'single',
  triggersEnter: [AccountsTemplates.ensureSignedIn],
  action: function(params, queryParams) {
    BlazeLayout.render('masterLayout', {
      footer: 'footer',
      main: 'singleCamera',
      nav: 'nav',
    });
  }
});
*/
FlowRouter.route('/privacy', {
  name: 'privacy',
  action: function(params, queryParams) {
    BlazeLayout.render('masterLayout', {
      footer: 'footer',
      main: 'privacyPage',
      nav: 'nav',
    });
  }
});

FlowRouter.route('/terms-of-use', {
  name: 'terms',
  action: function(params, queryParams) {
    BlazeLayout.render('masterLayout', {
      footer: 'footer',
      main: 'termsPage',
      nav: 'nav',
    });
  }
});


FlowRouter.route('/history', {
  name: 'history',
  triggersEnter: [AccountsTemplates.ensureSignedIn],
  action: function(params, queryParams) {
    BlazeLayout.render('masterLayout', {
      footer: 'footer',
      main: 'history',
      nav: 'nav',
    });
  }
});

FlowRouter.route('/timeline/:nickname', {
  name: 'timeline',
  triggersEnter: [AccountsTemplates.ensureSignedIn],
  action: function(params, queryParams) {
    console.log(params);
    console.log("FlowRouter.route('/timeline/:nickname'");
    console.log(Session.get('currentNeighborhood'));
    BlazeLayout.render('masterLayout', {
      footer: 'footer',
      main: 'timeline',
      nav: 'nav',
    });
  }
});

FlowRouter.route('/faq', {
  name: 'faq',
  triggersEnter: [AccountsTemplates.ensureSignedIn],
  action: function(params, queryParams) {
    BlazeLayout.render('masterLayout', {
      footer: 'footer',
      main: 'faq',
      nav: 'nav',
    });
  }
});

FlowRouter.route('/user-manual', {
  name: 'user-manual',
  //triggersEnter: [AccountsTemplates.ensureSignedIn],
  action: function(params, queryParams) {
    BlazeLayout.render('masterLayout', {
      footer: 'footer',
      main: 'userManualPage',
      nav: 'nav',
    });
  }
});

FlowRouter.route('/about', {
  name: 'about',
  //triggersEnter: [AccountsTemplates.ensureSignedIn],
  action: function(params, queryParams) {
    BlazeLayout.render('masterLayout', {
      footer: 'footer',
      main: 'aboutPage',
      nav: 'nav',
    });
  }
});

FlowRouter.notFound = {
  action: function() {
    BlazeLayout.render('masterLayout', {
      footer: 'footer',
      main: 'pageNotFound',
      nav: 'nav',
    });
  }
};

//Routes
AccountsTemplates.configureRoute('changePwd');
AccountsTemplates.configureRoute('forgotPwd');
AccountsTemplates.configureRoute('resetPwd');
AccountsTemplates.configureRoute('signIn');
AccountsTemplates.configureRoute('signUp');
AccountsTemplates.configureRoute('verifyEmail');
