Neighborhoods = new Mongo.Collection('Neighborhoods');
Cameras = new Mongo.Collection('Cameras');
AllowedEmails = new Mongo.Collection('AllowedEmails');
Videos = new Mongo.Collection('Videos');
OldVideos = new Mongo.Collection('OldVideos');
Images = new Mongo.Collection('Images');

AllowedEmails.allow({
  insert: function (userId, doc) {
    // the user must be logged in, and the document must be owned by the user
    return true;//(userId && doc.owner === userId);
  },
  remove: function (userId, doc) {
    // the user must be logged in, and the document must be owned by the user
    return true;//(userId && doc.owner === userId);
  }
  //fetch: function (userId, doc) {
    // the user must be logged in, and the document must be owned by the user
  //  return true;//(userId && doc.owner === userId);
  //}
});

Cameras.allow({
  insert: function (userId, doc) {
    // the user must be logged in, and the document must be owned by the user
    return true;//(userId && doc.owner === userId);
  },
  remove: function (userId, doc) {
    // the user must be logged in, and the document must be owned by the user
    return true;//(userId && doc.owner === userId);
  }
  //fetch: function (userId, doc) {
    // the user must be logged in, and the document must be owned by the user
  //  return true;//(userId && doc.owner === userId);
  //}
});


Neighborhoods.allow({
  insert: function (userId, doc) {
    // the user must be logged in, and the document must be owned by the user
    return true;//(userId && doc.owner === userId);
  },
  update: function (userId, doc) {
    // the user must be logged in, and the document must be owned by the user
    return true;//(userId && doc.owner === userId);
  }
});

Meteor.methods({
  'allowedEmails.add'({ email, neighborhood }) {
    console.log('Adding email ' + email + ' to ' + neighborhood);
    Neighborhoods.upsert(
      { name: neighborhood}, 
      { $push: { emails: email } }
    );
    AllowedEmails.upsert(
      { email: email },
      { $push: { neighborhood: neighborhood } }
    );
  },
  'userIsAdminForNeighborhood'() {
    return true;
  }
});
