// Options
AccountsTemplates.configure({
  defaultLayout: 'masterLayout',
  defaultLayoutRegions: {
    nav: 'nav',
    footer: 'footer',
  },
  defaultContentRegion: 'main',
  showForgotPasswordLink: true,
  overrideLoginErrors: true,
  enablePasswordChange: true,

  sendVerificationEmail: true,
  enforceEmailVerification: true,
  confirmPassword: false,
  //continuousValidation: false,
  //displayFormLabels: true,
  //forbidClientAccountCreation: true,
  //formValidationFeedback: true,
  //showAddRemoveServices: false,
  //showPlaceholders: true,
  showForgotPasswordLink: true,
  //showResendVerificationEmailLink: true,
  showLabels: false,


  negativeValidation: true,
  positiveValidation: true,
  negativeFeedback: false,
  positiveFeedback: true,

  homeRoutePath: '/dashboard',

  // Privacy Policy and Terms of Use
  privacyUrl: 'privacy',
  termsUrl: 'terms-of-use',
//});

//AccountsTemplates.configure({
    texts: {
      title: {
        changePwd: "Password Title",
        enrollAccount: "Enroll Title",
        forgotPwd: "Forgot Pwd Title",
        resetPwd: "Reset Pwd Title",
        signIn: "",//Sign in to view live feeds",
        signUp: "Sign Up Title",
        verifyEmail: "Verify Email Title",
      },
      button: {
        signUp: "Create Account",
        forgotPwd: "Reset Password"

      },
      errors: {
        loginForbidden: "Invalid email or password",
        verifyEmailFirst: "Please verify your email first; check the email and follow the link!"
      },
      termsPreamble: "clickAgree",
      termsPrivacy: "privacyPolicy",
      termsAnd: "and",
      termsTerms: "terms",
    }
});

AccountsTemplates.removeField('password');
AccountsTemplates.addField({
    _id: 'password',
    type: 'password',
    required: true,
    minLength: 7,
    //re: /(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/,
    //errStr: 'At least 1 digit, 1 lower-case and 1 upper-case',
});
/* this can be optionally added
AccountsTemplates.addField({
    _id: 'name',
    type: 'text',
    displayName: 'Name',
    func: function(value){return value !== 'Full Name';},
    errStr: 'Only "Full Name" allowed!',
});
*/
var myPostLogout = function() {
    //console.log('myPostLogout');
    FlowRouter.go('/sign-in');
};
AccountsTemplates.configure({
    onLogoutHook: myPostLogout
});

var postSignUpFunc = function() {   // Never gets called!!!!!
};


var mySubmitFunc = function(error, state){
  if (!error) {
    if (state === "signIn") {
      // Successfully logged in
      //Meteor.logoutOtherClients(function callback() {console.log('logoutOtherClients callback');});
      FlowRouter.redirect('/dashboard');
    }
    if (state === "signUp") {
      // Successfully registered
      AccountsTemplates.setState("verifyEmail");
    }
  }
  else {
    AccountsTemplates.state.form.set("message", null);
    if (error.reason.includes('Email already exists')) {
      error.reason = error.reason.replace(".", "");
    }
    if (state == 'resendVerificationEmail') {
    }
  }
};

var myPreSignUpHook = function(password, info) {
  AccountsTemplates.sendingVerificationEmail = true;
  AccountsTemplates.state.form.set("message", "Sending verification email ... please wait");
}

AccountsTemplates.configure({
    onSubmitHook: mySubmitFunc,
    preSignUpHook: myPreSignUpHook,
    postSignUpHook: postSignUpFunc
});

