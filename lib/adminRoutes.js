function hasAdminRights() {
  return Meteor.user();// && Meteor.user().isAdmin;
}

FlowRouter.route('/admin', {
  name: 'admin',
  triggersEnter: [AccountsTemplates.ensureSignedIn],
  action: function(params, queryParams) {
    BlazeLayout.render('masterLayout', {
      footer: 'footer',
      main: 'adminOrLogin',
      nav: 'nav',
    });
  }
});
/*
FlowRouter.route('/emails', {
  name: 'emailsView',
  triggersEnter: [AccountsTemplates.ensureSignedIn],
  action: function(params, queryParams) {
    if (hasAdminRights()) {
      BlazeLayout.render('masterLayout', {
        footer: 'footer',
        main: 'emailsView',
        nav: 'nav',
      });
    } else {
      BlazeLayout.render('masterLayout', {
        footer: 'footer',
        main: 'pageDenied',
        nav: 'nav',
      });
    }
  }
});

FlowRouter.route('/cameras', {
  name: 'cameraSetupView',
  triggersEnter: [AccountsTemplates.ensureSignedIn],
  action: function(params, queryParams) {
    if (hasAdminRights()) {
      BlazeLayout.render('masterLayout', {
        footer: 'footer',
        main: 'cameraSetupView',
        nav: 'nav',
      });
    } else {
      BlazeLayout.render('masterLayout', {
        footer: 'footer',
        main: 'pageDenied',
        nav: 'nav',
      });
    }
  }
});
*/